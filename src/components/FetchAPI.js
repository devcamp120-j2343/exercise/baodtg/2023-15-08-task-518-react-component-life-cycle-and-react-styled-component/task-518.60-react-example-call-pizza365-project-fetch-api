import { Component } from "react";

class FetchAPI extends Component {
    fetchAPI = async (url, requestOptions) => {
        let response = await fetch(url, requestOptions);
        let data = await response.json();
        return data

    }
    onBtnGetAllOrdersClick = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        var url = "http://203.171.20.210:8080/devcamp-pizza365/orders"

        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })
    }

    onBtnCreateOrderClick = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "kichCo": "M",
            "duongKinh": "25",
            "suon": 4,
            "salad": "300",
            "loaiPizza": "HAWAII",
            "idVourcher": "16512",
            "thanhTien": 200000,
            "giamGia": 20000,
            "idLoaiNuocUong": "PEPSI",
            "soLuongNuoc": 3,
            "hoTen": "Phạm Thanh Bình",
            "email": "binhpt001@devcamp.edu.vn",
            "soDienThoai": "0865241654",
            "diaChi": "Hà Nội",
            "loiNhan": "Pizza đế dày",
            "trangThai": "cancel",
            "ngayTao": 1692451053251,
            "ngayCapNhat": 1692513781814
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        var url = "http://203.171.20.210:8080/devcamp-pizza365/orders"
        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })


    }

    onBtnGetOrderByIdClick = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        var url = "http://203.171.20.210:8080/devcamp-pizza365/orders/S13BmgSHmb"
        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })

    }

    onBtnUpdateOrderByIdClick = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "trangThai": "confirmed"
        });

        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        var url = "http://203.171.20.210:8080/devcamp-pizza365/orders/185234";
        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })

    }


    onBtnCheckVoucherByIdClick = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        var url = "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/24864";
        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })

    }


    onBtnGetDrinkListClick = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        var url = "http://203.171.20.210:8080/devcamp-pizza365/drinks";

        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })

    }


    render() {
        return (
            <div className="row mt-5 px-5">
                <div className="col ">
                    <button className="btn btn-primary ms-5" onClick={this.onBtnGetAllOrdersClick}>Call api to get all orders</button>
                </div>
                <div className="col ">
                    <button className="btn btn-info" onClick={this.onBtnCreateOrderClick}>Call api to create orders</button>
                </div>
                <div className="col ">
                    <button className="btn btn-success" onClick={this.onBtnGetOrderByIdClick}>Call api to get order by id</button>
                </div>
                <div className="col ">
                    <button className="btn btn-secondary" onClick={this.onBtnUpdateOrderByIdClick}>Call api to update orders</button>
                </div>
                <div className="col ">
                    <button className="btn btn-danger" onClick={this.onBtnCheckVoucherByIdClick}>Call api to check voucher by id</button>
                </div>
                <div className="col ">
                    <button className="btn btn-warning" onClick={this.onBtnGetDrinkListClick}>Call api to get drink list</button>
                </div>
            </div>
        )
    }
}
export default FetchAPI