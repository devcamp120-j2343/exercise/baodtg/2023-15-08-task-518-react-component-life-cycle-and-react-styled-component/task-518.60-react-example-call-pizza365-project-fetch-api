import { Component } from "react";
import axios from "axios";

class AxiosAPI extends Component {
    axiousLibraryCallAPI = async (config) => {
        let response = await axios(config);
        return response.data

    }
    onBtnGetAllOrdersClick = () => {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders',
            headers: {}
        };

        this.axiousLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })
    }

    onBtnCreateOrderClick = () => {
        let data = JSON.stringify({
            "kichCo": "M",
            "duongKinh": "25",
            "suon": 4,
            "salad": "300",
            "loaiPizza": "HAWAII",
            "idVourcher": "16512",
            "thanhTien": 200000,
            "giamGia": 20000,
            "idLoaiNuocUong": "PEPSI",
            "soLuongNuoc": 3,
            "hoTen": "Phạm Thanh Bình",
            "email": "binhpt001@devcamp.edu.vn",
            "soDienThoai": "0865241654",
            "diaChi": "Hà Nội",
            "loiNhan": "Pizza đế dày"
        });
        let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };
        this.axiousLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })


    }

    onBtnGetOrderByIdClick = () => {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders/eUMHX8PyNX',
            headers: {}
        };

        this.axiousLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })

    }

    onBtnUpdateOrderByIdClick = () => {
        let data = JSON.stringify({
            "trangThai": "confirmed"
        });

        let config = {
            method: 'put',
            maxBodyLength: Infinity,
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders/185235',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };
        this.axiousLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })

    }


    onBtnCheckVoucherByIdClick = () => {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: 'http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/24864',
            headers: { }
          };
        this.axiousLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })

    }


    onBtnGetDrinkListClick = () => {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: 'http://203.171.20.210:8080/devcamp-pizza365/drinks',
            headers: { }
          };

        this.axiousLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })

    }


    render() {
        return (
            <div className="row mt-5 px-5">
                <div className="col ">
                    <button className="btn btn-primary ms-5" onClick={this.onBtnGetAllOrdersClick}>Call api to get all orders</button>
                </div>
                <div className="col ">
                    <button className="btn btn-info" onClick={this.onBtnCreateOrderClick}>Call api to create orders</button>
                </div>
                <div className="col ">
                    <button className="btn btn-success" onClick={this.onBtnGetOrderByIdClick}>Call api to get order by id</button>
                </div>
                <div className="col ">
                    <button className="btn btn-secondary" onClick={this.onBtnUpdateOrderByIdClick}>Call api to update orders</button>
                </div>
                <div className="col ">
                    <button className="btn btn-danger" onClick={this.onBtnCheckVoucherByIdClick}>Call api to check voucher by id</button>
                </div>
                <div className="col ">
                    <button className="btn btn-warning" onClick={this.onBtnGetDrinkListClick}>Call api to get drink list</button>
                </div>
            </div>
        )
    }
}
export default AxiosAPI