import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import FetchAPI from './components/FetchAPI';
import AxiosAPI from './components/AxiosAPI';

function App() {
  return (
    <div >
      <FetchAPI />
      <hr></hr>
      <AxiosAPI />
    </div>
  );
}

export default App;
